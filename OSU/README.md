# OSU results

This folder holds raw data and visual representation of OSU results, obtained on
a real-world cluster, an S4BXI simulation and an SMPI simulation.

Raw data is available in CSV format in the corresponding folders. The graphs for
all 3 datasets (1 real and 2 simulated) are in the SMPI folder. Each file is
named in the format `{benchmark name}_{process number}`, the number of processes
and number of machines are the same since all tests were executed with 1 process
per node.

## Context

**Real-world cluster:** BXI v2 inteconnect, AMD EPYC™ 7763 64-Core CPU, 264GB
RAM

**Simulation machine:** Regular desktop computer with an Intel® Core™ i9-10850K
CPU and 32GB RAM

## Versions used

**SimGrid (including SMPI):** commit
[30545f2d](https://framagit.org/simgrid/simgrid/-/tree/30545f2dfbcbb2c4095a7be9507575c624bdd6df)

**S4BXI:** commit
[1acf4bae](https://framagit.org/s4bxi/s4bxi/-/tree/1acf4bae4071001bc06af94da3a252f508e1564a)

**OpenMPI:** commit cefa21b51b (based on version 4.1.0.1)

**OSU micro-benchmarks:** version 5.6.3

## Simulators configuration

**SMPI:** configuration obtained using the official [platform
calibration](https://framagit.org/simgrid/platform-calibration) (commit
[28f1d001](https://framagit.org/simgrid/platform-calibration/-/tree/28f1d001cfb2c2ee6cd99b440022f239b345f575))

**Environment variables set for S4BXI:**

- S4BXI_E2E_OFF=0
- S4BXI_KEEP_TEMPS=0
- S4BXI_CPU_ACCUMULATE=0
- S4BXI_QUICK_ACKS=0
- S4BXI_PRIVATIZE_LIBS=libmpi.so.40;libopen-rte.so.40;libopen-pal.so.40
- S4BXI_MODEL_PCI_COMMANDS=1
- S4BXI_CPU_THRESHOLD=1e-8
- S4BXI_CPU_FACTOR=0.3
