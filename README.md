# S4BXI *MPI paper*'s data

This repository holds the raw data and some graphs used in our paper presenting
MPI simulation on top of S4BXI, using OpenMPI. See the README.md file in
subfolders for more infos