# Context
*17/06/2021 17:07*

## simgrid

1a6e755b80 Ooops, fmt is second arg.

## s4bxi

1acf4ba Fix process-level flow control

## ompi

cefa21b51b More things we don't need to build

## LULESH

c9fc0aa Add flow control tests

## Environment

S4BXI_PATH=/opt/s4bxi
S4BXI_E2E_OFF=0
S4BXI_KEEP_TEMPS=0
S4BXI_CPU_ACCUMULATE=0
S4BXI_PRIVATIZE_LIBS=libmpi.so.40;libopen-rte.so.40;libopen-pal.so.40
S4BXI_CPU_THRESHOLD=1e-8
S4BXI_CPU_FACTOR=1.2
