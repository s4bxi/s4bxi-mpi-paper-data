# LULESH results

This folder holds raw data and visual representation of LULESH results, obtained on
a real-world cluster and S4BXI simulation.

## Context

**Real-world cluster:** BXI v2 inteconnect 
- **Intel folder:** Intel®'s Knight Landing CPUs (Xeon Phi™ 7250), 200GB RAM
- **AMD folder:** AMD EPYC™ 7763 64-Core CPU, 264GB RAM

**Simulation machine:** Regular desktop computer with an Intel® Core™ i9-10850K
CPU and 32GB RAM

## Versions used

**SimGrid:** commit
[1a6e755b80](https://framagit.org/simgrid/simgrid/-/tree/1a6e755b803dd31c7e1a2530baed0c2e73187119)

**S4BXI:** commit
[1acf4ba](https://framagit.org/s4bxi/s4bxi/-/tree/1acf4bae4071001bc06af94da3a252f508e1564a)

**OpenMPI:** commit cefa21b51b (based on version 4.1.0.1)

**LULESH:** commit
[3e01c40](https://github.com/LLNL/LULESH/tree/3e01c40b3281aadb7f996525cdd4a3354f6d3801)

## Simulators configuration

**Environment variables set for S4BXI:**

- S4BXI_E2E_OFF=0
- S4BXI_KEEP_TEMPS=0
- S4BXI_CPU_ACCUMULATE=0
- S4BXI_PRIVATIZE_LIBS=libmpi.so.40;libopen-rte.so.40;libopen-pal.so.40
- S4BXI_CPU_THRESHOLD=1e-8
- S4BXI_CPU_FACTOR=15
